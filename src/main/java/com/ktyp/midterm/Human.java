package com.ktyp.midterm;

public class Human {
    private String name;
    public int health;
    public int education;
    public int happiness;
    public int money;

    public Human(String name, int health, int education, int happiness, int money) {
        this.name = name;
    }

    public void print(String string) {
        System.out.println(getName() + " health: " + gethealth() + " education: " + getEducation() + " happiness: "
                + getHappiness() + " money: " + getMoney());
    }

    public int gethealth() {
        return health;
    }

    public int getEducation() {
        return education;
    }

    public int getHappiness() {
        return happiness;
    }

    public int getMoney() {
        return money;
    }

    public String getName() {
        return name;
    }

    public void health() {
        if (money == 0) {
            System.out.println("--------------Your don't have money--------------");
            return;
        } else {
            health = health + 4;
            money = money - 2;
            return;
        }
    }

    public void education() {
        education = education + 4;
        return;

    }

    public void happiness() {
        happiness = happiness + 4;
        return;

    }

    public void money() {
        money = money + 4;
        return;
    }

}
