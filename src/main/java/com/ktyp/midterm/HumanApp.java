package com.ktyp.midterm;

import java.util.Scanner;

public class HumanApp {
    static Human Player1 = new Human("Player1 : ", 0, 0, 0, 0);
    static Human Player2 = new Human("Player2 : ", 0, 0, 0, 0);
    static Human Player3 = new Human("Player3 : ", 0, 0, 0, 0);
    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }

    /*
     * กด 1 = เพิ่มค่า health
     * กด 2 = เพิ่มค่า education
     * กด 3 = เพิ่มค่า happiness
     * กด 4 = เพิ่มค่า money
     * กด 0 = ออกจากเกม
     */

    public static void process(String command) {
        switch (command) {
            case "1":
                Player1.health();
                Player2.health();
                Player3.health();
                break;
            case "2":
                Player1.education();
                Player2.education();
                Player3.education();
                break;
            case "3":
                Player1.happiness();
                Player2.happiness();
                Player3.happiness();
                break;
            case "4":
                Player1.money();
                Player2.money();
                Player3.money();
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;

        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            while (true) {
                String command = input();
                process(command);
                Player1.print(command);
                break;
            }
        }
        System.out.println("-------------------------Game Over-------------------------");
        Player2.health = 0;
        Player2.education = 0;
        Player2.happiness = 0;
        Player2.money = 0;
        for (int i = 0; i < 10; i++) {
            while (true) {
                String command = input();
                process(command);
                Player2.print(command);
                break;
            }
        }
        System.out.println("-------------------------Game Over-------------------------");
        Player3.health = 0;
        Player3.education = 0;
        Player3.happiness = 0;
        Player3.money = 0;
        for (int i = 0; i < 10; i++) {
            while (true) {
                String command = input();
                process(command);
                Player3.print(command);
                break;
            }
        }
        System.out.println("-------------------------Game Over-------------------------");
    }
}
