package com.ktyp.midterm;

import java.util.Scanner;

public class WalkApp {
    static Map map = new Map(9, 10);
    static Walk donaldduck = new Walk(map, 'D', 0, 1);
    static Walk fethryduck = new Walk(map, 'F', 0, 4);
    static Walk mofyduck = new Walk(map, 'M', 0, 7);
    static Stone stone1 = new Stone(map, 0, 10);
    static Stone stone2 = new Stone(map, 0, 9);
    static Stone stone3 = new Stone(map, 1, 2);
    static Stone stone4 = new Stone(map, 1, 7);
    static Stone stone5 = new Stone(map, 2, 5);
    static Stone stone6 = new Stone(map, 3, 6);
    static Stone stone7 = new Stone(map, 4, 3);
    static Stone stone8 = new Stone(map, 4, 8);
    static Stone stone9 = new Stone(map, 5, 2);
    static Stone stone10 = new Stone(map, 5, 5);
    static Stone stone11 = new Stone(map, 6, 2);
    static Stone stone12 = new Stone(map, 6, 9);
    static Stone stone13 = new Stone(map, 8, 4);
    static Stone stone14 = new Stone(map, 7, 8);
    static Stone stone15 = new Stone(map, 6, 10);
    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }

    public static void process(String command) {
        switch (command) {
            case "w":
            donaldduck.up();
            fethryduck.up();
            mofyduck.up();
                break;
            case "s":
            donaldduck.down();
            fethryduck.down();
            mofyduck.down();
                break;
            case "d":
            donaldduck.right();
            fethryduck.right();
            mofyduck.right();
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;

        }
    }

    public static void main(String[] args) {

        map.add(stone1);
        map.add(stone2);
        map.add(stone3);
        map.add(stone4);
        map.add(stone5);
        map.add(stone6);
        map.add(stone7);
        map.add(stone8);
        map.add(stone9);
        map.add(stone10);
        map.add(stone11);
        map.add(stone12);
        map.add(stone13);
        map.add(stone14);
        map.add(stone15);
        map.add(donaldduck);
        map.add(fethryduck);
        map.add(mofyduck);

        while(true) {
            map.print();
            String command = input();
            process(command);

        }
    }
}
