package com.ktyp.midterm;

public class Walk extends Unit{
    public Walk(Map map, char symbol, int x, int y) {
        super(map, symbol, x, y, false);
    }
    public void up() {
        int y = this.getY();
        y--;
        this.setY(y);
    }
    public void down() {
        int y = this.getY();
        y++;
        this.setY(y);
    }
    public void right() {
        int x = this.getX();
        x++;
        this.setX(x);
    }

}
